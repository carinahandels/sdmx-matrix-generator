# 1.4 beta 27/03/2023
- Big change to constraint definitions - simpler and faster!
    - Constraints are completely defined in the Matrix worksheet. Multiple codes are listed in the cell instead of using %
    - Multiple codes can be entered directly into matrix cells, and/or:
    - a right-click pop-up form can be used to select the constrained codes. Ids and Names can be searched
    - Constraint definitions removed from Codelist worksheets
    - Constraint "helper" buttons are removed as they're not needed now
- Prefill parameters allow selective importing of artefacts. E.g. import a DSD but leave the codelists in place
- Optionally generate and prefill Decomposition as DECOMPOSITION annotation on Concept Scheme
- Parser and prefill of Annotations with AnnotationTexts
- Allow uncoded dimensions
- Fixed prefilling accents from file share by using ADO.Stream used instead of Scripting.FileSystemObject
- Less message boxes when prefilling and generating
- Stability and error-trapping enhancements

# 1.3.2 07/02/2023
- Synchronise the name changes of DSDs, Dataflows, and Concept names across the worksheets
- For surveys, now works with up to 300 concepts, >1000 dataflows
- HCLs: 
    - Can now prefill and generate completely flat hierarchies
    - Prefill HCL with no other artefact now works
- Worksheet formatting and alignments after Prefill is improved
- Performance improvements
- Several bug fixes

# 1.3.1 19/01/2023
- Stability release. Several bug fixes and resilience improvements

# 1.3 28/11/2022
- Change to the worksheet order and names to follow the SDMX modelling guidelines and the new .Stat academy modelling course
- The decomposition, matrix and constraints now uses names for Dataflows and DSDs instead of IDs. It makes modelling easier
- Reduced the workbook file size as it caused problems for some users
- Several bug fixes
- Updated the user guide to match the new design

# 1.2.2 16/9/2022
- Fix: Wrong agency in the referenced code list in the HCL generated from Matrix Generator
- Fix: Incorrect parsing of hierarchical codes
- Fix: Error when pre-filling the HCL code list
- Fix: Ensure constraintID uses correct codelist
- Better error descriptions when IDs not aligned
- Fix: Crash when loading DSD produced with FMR
- Fix: Bug in getting Constraint Id from Dataflows worksheet
- Add possibility to generate METADATA annotation for DSD

# 1.2.1 29/6/2022
- Ensure constraintID uses correct codelist
- Bug in getting Constraint Id from Dataflows worksheet
- Add possibility to generate METADATA annotation for DSD
- Error trapping for mismatched codes in DSD, Dataflow, Matrix worksheets

# 1.2 25/5/2022
- Added Hierarchical Codelist template, generation and prefill
- Enabled custom Annotations in DSD and Dataflows that use JSON syntax

# v1.1.3  31/3/2022
## Fixed
- Enabled insert/delete rows on worksheets
- Made the dynamic named ranges more resistant to editing changes
- Fixed group dimensions in CS when local representations change concept type
- Fixed constraint generation: the value type can be overridden from local representations
- Fix to removed URN requirement when prefilling
- Improved robustness of reporting template generation
- Change CL_UNIT_MULT descriptions to superscript exponents
- CL_FREQ updated to 2.1

# v1.1.2  15/3/2022
## Fixed
- Made the dynamic named ranges more resistant to editing changes

# v1.1.1  9/3/2022
## Fixed
- Set all IsFinal fields features updating wrong columns, same for Set all Agency… 
- CS attribute minlength was compared to the DSD attribute maxlength
- Position of TIME_PERIOD being shown in Local representation. Stopped this because it’s always generated as the last dimension and position is ignored in Concept Scheme

# v1.1.0  8/3/2022
## New
- Added ability to overload each DSD's local representations. Prefill works also
- Can selectively generation DSDs and DFs on their worksheets
- Create artificial, incrementing key on each row of the generated Reporting Template for concepts with a Concept role of COUNT
- The Constraint isFinal now matches the Dataflow isFinal
- Updated user guide, added the Local Representation details
- Include decomposition example on worksheet 2.Decomposition indicators
- Added the tool version number on Generate worksheet. Starting at 1.1.0 to denote this is not the first version but it is backwards compatible.
## Fixed
- Minor bug fixes

# 29/11/2021
- Added the Transform_template_to_sdmxcsv.py script to transform the reporting template to SDMX-CSV, explained in the user guide

# 10/11/2021
## New
- Existing codelists can be referenced with 3-part identifier in Concept Scheme. Avoids requiring a codelist worksheet
- Generate a reporting template with guidelines
- Substantial improvements to user guide 

## Fixed
- Dimension Position attribute behaviour changed:
    - Position values are not now auto-generated 
    - If there are Position values, 
        - all non-time dimensions must have Position
        - The order of the dimensions in the SDMX-ML follows the Position values
- Changed all isFinal cells to Text, hopefully avoids true/false being translated in SDMX-ML
- Changed all integer variables to long to avoid problems with long lists
- Disable Delete rows in DF, DSD and Concept Scheme worksheets to avoid corrupting named ranges
- Fixed duplicate ID conditional formatting for codelists
- Removed duplicate Name conditional formatting for codelists - it was confusing

# 20/8/2021
## New
- Add attribute group definitions in Concept Scheme worksheet (for DSDs)

## Fixed 
- Prefill errors if structure contains keyset content constraints
- Change IDs of microdata-related annotation types
- Other bug fixes and performance improvements

# 16/6/2021
## Fixed 
- Fix slow generation time (slow Find problem in constraints)

# 15/6/2021
## New
- Allow skipping the generation of codelists to avoid overwriting non-final codelists when collaborating
- Codelist code orders: Auto generate of order required when only a subset of codes have an order
- Add "MAXTEXTATTRIBUTELENGTH" column to the DSDs sheet

## Fixed
- Improved speed of prefilling codelists
- Change UNIT_MULT default attachment to series
- Matrix Generator highlights codes with red on CL sheets that are actually not duplicates
- Concept number in a concept scheme is used as dimension position ID if Position is empty on the Concept Scheme sheet

# 22/4/2021
## Fixed
- DSD to take more than the first 99 concepts by default
- Add "MAXTEXTATTRIBUTELENGTH" column to the DSDs sheet
- Change IDs of microdata-related annotation types
- Issues with generation of annotations
- Invalid SDMX code is generated when DSD structure has no attributes
- Dimensions Position attribute ignored when prefilling
- Increase allowed number of used concepts in a DSD to 999

# 10/3/2021
## Fixed
- Prefilling artefacts w/o isFinal attribute causes error
- Support of large codelists in Matrix Generator. Now supports up to 500000 codes.

# 27/11/2020
## New
- Add MAXTEXTATTRIBUTELENGTH DSD annotation
- Make Constraint IDs editable. When prefilling show them on Dataflow worksheet
- Add named ranges to make it easier for programs that wrap the application
- Add UNIT_MEASURE_CONCEPTS Dataflow annotation
- Add DSD annotations

## Fixed
- Dimension with role D(Frequency) is skipped when generating attachment relationship for time-series attribute [github#68](https://github.com/OECDSTD/sdmx-matrix-generator/issues/68)
- Prefill from Rest API goes in error with Excel language settings "German"
- Locale language setting in Excel different from English [github#53](https://github.com/OECDSTD/sdmx-matrix-generator/issues/53)
- Prefill function errors when regional settings include separators that are incompatible with adding hyperlinks
- Prefilling Annotations errors because it assumes several properties are mandatory. They're not in SDMX
